const port = chrome.extension.connect({
    name: "OnePass Popup Communication"
});

// send a request payload to the background service/script
function sendRequest(payload) {
    const valid = payload && 'request' in payload && 'params' in payload;
    if (valid) {
        port.postMessage(JSON.stringify(payload));
    } else {
        console.error("invalid request payload");
    }
}

function handleResponse(response) {
    response = JSON.parse(response);

    if (response.request === 'resume') {
        if (response.status == false) {
            console.log('resume session failed...');
            createLoginPage();
        } else {
            console.log('session resumed successfully');
            const data = response.data;
            window.user = {}
            window.user.email = data.email;
            window.user.firstName = data.firstName;
            window.user.lastName = data.lastName;
            createWelcomePage();
        }
    }

    if (response.request === 'login') {
        if (response.status == true) {

            let email = document.getElementById('email').value;
            let password = document.getElementById('password').value;
            const data = response.data;

            window.user = {}
            window.user.email = email;
            window.user.firstName = data.firstName;
            window.user.lastName = data.lastName;

            let masterkey = pbkdf2.pbkdf2Sync(password, email, 10000, 128, 'sha256').toString('hex');

            let encryptedKey = window.aes256.encrypt(data.sessionKey, masterkey);

            chrome.storage.local.set({ 'masterKey': encryptedKey }, () => {
                console.log('user logged in and master key stored!');
            });

            createWelcomePage();

        }
    }

    if (response.request === 'passwords') {
        if (response.status == true) {
            listPasswords(response.data.sessionKey, response.data.passwords);
        }
    }
}

createLoginPage = () => {
    let emailTitle = document.createElement('h4');
    emailTitle.innerText = 'Email:';
    let emailInput = document.createElement('input');
    emailInput.autocomplete = 'off';
    emailInput.type = 'email';
    emailInput.name = 'email';
    emailInput.id = 'email';

    let passwordTitle = document.createElement('h4');
    passwordTitle.innerText = 'Master Password:';
    let passInput = document.createElement('input');
    passInput.type = 'password';
    passInput.name = 'password';
    passInput.id = 'password';

    let submitBtn = document.createElement('button');
    submitBtn.innerText = "Login";
    submitBtn.id = 'login-btn';
    submitBtn.classList.add('submit-button');

    let centerDiv = document.createElement('div');
    centerDiv.classList.add('center');
    centerDiv.appendChild(submitBtn);

    submitBtn.onclick = () => {
        let email = document.getElementById('email').value;
        let password = document.getElementById('password').value;

        let masterkey = pbkdf2.pbkdf2Sync(password, email, 10000, 128, 'sha256').toString('hex');

        let verifyHash = pbkdf2.pbkdf2Sync(masterkey, password, 10000, 64, 'sha256').toString('base64');

        const data = {
            email: email,
            verifyHash: verifyHash
        }

        sendRequest({
            request: 'login',
            params: [data]
        });
    }

    let content = document.getElementById('content');
    content.innerHTML = null;

    content.appendChild(emailTitle);
    content.appendChild(emailInput);
    content.appendChild(passwordTitle);
    content.appendChild(passInput);
    content.appendChild(centerDiv);

}


createWelcomePage = () => {

    let content = document.getElementById('content');
    content.innerHTML = null;

    let welcomeUser = document.createElement('h4');

    welcomeUser.innerText = `Welcome back, ${window.user.firstName} ${window.user.lastName} !`;

    let passwordsList = document.createElement('div');
    passwordsList.id = "passwords-list";
    passwordsList.classList.add('list-items');

    content.appendChild(welcomeUser);
    content.appendChild(passwordsList);

    sendRequest({
        request: "passwords",
        params: []
    });
}

function listPasswords(sessionKey, passwords) {
    chrome.tabs.query({ active: true, currentWindow: true }, (tabs) => {
        const current_tab = tabs[0];
        const url = current_tab.url.split('/').slice(0, 3).join('/');
        const passwordList = document.getElementById('passwords-list');

        chrome.storage.local.get(['masterKey'], (result) => {
            let masterKey = result.masterKey;
            masterKey = aes256.decrypt(sessionKey, masterKey);


            let urlTitle = document.createElement('h4');
            urlTitle.innerText = url;

            passwordList.appendChild(urlTitle);

            passwords.forEach(element => {
                if (element.url == url) {
                    let passwordItem = document.createElement('div');
                    passwordItem.classList.add('password-item');

                    let usernameText = document.createElement('p');
                    usernameText.innerText = `username: ${element.username}`;

                    let categoryText = document.createElement('p');
                    categoryText.innerText = `category: ${element.category}`;

                    let copyBtn = document.createElement('button');
                    copyBtn.innerText = "Copy Pass";

                    copyBtn.onclick = () => {
                        navigator.clipboard.writeText(aes256.decrypt(masterKey, element.pass)).then(function () {
                            console.log('password successfully copied to clipboard');
                        }, function (err) {
                            console.error('failed to copy to clipboard: ', err);
                        });
                    }

                    passwordItem.appendChild(usernameText);
                    passwordItem.appendChild(categoryText);
                    passwordItem.appendChild(copyBtn);

                    passwordList.appendChild(passwordItem);
                }
            });
        });
    })
}

// process incoming payload response from background service/script
port.onMessage.addListener(function (msg) {
    handleResponse(msg);
});

window.onload = () => {

    // try to resume session if exists
    sendRequest({
        request: "resume",
        params: []
    });

}