console.log('OnePass background process loaded...');

const baseURL = "https://47f36d2e9f3a.ngrok.io/api";

function getURL(url){
    return `${baseURL}/${url}`;
}

function handleRequest(port, payload){

    payload = JSON.parse(payload);

    if(payload.request === 'resume'){
        fetch(getURL('user/session/key'), {
            credentials: 'include'
        }).then(res => {
            if(!res.ok){
                throw Error('unauthorized');
            }else{
                fetch(getURL('user'), {
                    credentials: 'include'
                }).then( res => res.json()).then(json => {
                    sendResponse(port, {
                        request: payload.request,
                        status: true,
                        message: 'user already logged in',
                        data : json.data
                    })
                })
            }
        }).catch( err => {
            handleError(port, payload.request, err.message);
        })
    }

    if(payload.request === 'login'){
        const data = payload.params[0];


        fetch(getURL('login'), {
            credentials: 'include',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            method: 'POST',
            body: JSON.stringify(data)
        }).then( res => {
            if(!res.ok){
                throw Error(res.statusText)
            }

            return res.json();
        }).then( json => {
            sendResponse(port, {
                request: payload.request,
                message: json.message,
                status: true,
                data : {
                    firstName: json.firstName,
                    lastName: json.lastName,
                    sessionKey: json.sessionKey
                }
            })
        }).catch( err => {
            handleError(port, payload.request, err.message);
        })
    }

    if(payload.request === 'passwords'){
        fetch(getURL('user/session/key'), {
            credentials: 'include'
        }).then(res => res.json()).then( json => {
            const sessionKey = json.data.sessionKey;
            fetch(getURL('user/password'), {
                credentials: 'include'
            }).then(res => res.json()).then(json => {
                sendResponse(port, {
                    request: payload.request,
                    status: true,
                    message: 'passwords loaded succussfully',
                    data: {
                        passwords: json.data,
                        sessionKey: sessionKey
                    }
                })
            })
        })
    }
}

function handleError(port, request, message){
    sendResponse(port, {
        request: request,
        status: false,
        message: message
    })
}

function sendResponse(port, response){

    const valid = response && 'request' in response && 'status' in response && 'message' in response;
    
    if(valid){
        port.postMessage(JSON.stringify(response));
    }else{
        console.error("invalid payload response");
    }
}

// communicate with the popup extension to exchange data
chrome.extension.onConnect.addListener(function(port) {
    port.onMessage.addListener(function(msg) {
        handleRequest(port, msg);
    });
})

// inject the foreground when tab is loaded
chrome.tabs.onUpdated.addListener( (tabId, info) => {
    if(info.status === 'complete'){
        chrome.tabs.get(tabId, current_tab_info => {
            if(/http:\/\/*|https:\/\/*/.test(current_tab_info.url)){
                chrome.tabs.executeScript(null, {file : './foreground.js'}, () => {
                    console.log(`foreground script injected in ${current_tab_info.url}`);
                })
            }
        })
    }
})
