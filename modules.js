const aes256 = require('aes256');
const pbkdf2 = require('pbkdf2');
const axios = require('axios').default;

global.window.aes256 = aes256;
global.window.pbkdf2 = pbkdf2;
global.window.axios = axios;